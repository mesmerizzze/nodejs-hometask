const { fighter } = require('../models/fighter');
const isEmpty = require('lodash.isempty')

const createFighterValid = (req, res, next) => {
    const { name, power, defense } = req.body;
    try {
        if (isEmpty(req.body)) {
            throw Error('No fighter data provided');
        }
        if (req.body.hasOwnProperty('id')) {
            throw Error('Uncorrect field')
        }
        if (!name) {
            throw Error('No fighter name provided');
        }
        if (!power || !Number.isInteger(power) || power < 0 || power > 100 ) {
            throw Error('Power value is uncorrect');
        }
        if (!defense || !Number.isInteger(defense) || defense < 1 || defense > 10) {
            throw Error('Defense value is uncorrect');
        }
        res.data = { name, power, defense };
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    const { name, power, defense } = req.body;
    let newData = {};
    try {
        if (req.body.hasOwnProperty('id')) {
            throw Error('Uncorrect field')
        }
        if (name) {
            newData.name = name;
        }
        if (power) {
            if (Number(power) < 1 || Number(power) < 0 || Number(power) > 100) {
                throw Error('Power value is uncorrect');
            }
            newData.power = Number(power);
        }
        if (defense) {
            if (Number(defense) < 1 || Number(defense) > 10) {
                throw Error('Defense value is uncorrect');
            }
            newData.defense = Number(defense);
        }
        res.data = newData;
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;