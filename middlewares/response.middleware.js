const responseMiddleware = (req, res, next) => {
    if (!res.err) {
        res.status(200);
        res.json(res.data);
    } else {
        res.json({
            error: true,
            message: res.err.message
        });
    }
}

exports.responseMiddleware = responseMiddleware;