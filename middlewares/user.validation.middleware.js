const { user } = require('../models/user');
const isEmpty = require('lodash.isempty');
const { fieldValidator } = require('../services/fieldValidator')

const createUserValid = (req, res, next) => {
    const { firstName, lastName, email, phoneNumber, password } = req.body
    try {
        if (isEmpty(req.body)) {
            throw Error('No user data provided');
        }
        if (req.body.hasOwnProperty('id')) {
            throw Error('Uncorrect field')
        }
        if (!firstName) {
            throw Error('No user name provided');
        }
        if (!lastName) {
            throw Error('No user last name provided');
        }
        if (!email || !fieldValidator(email, 'email')) {
            throw Error('User email is uncorrect');
        }
        if (!phoneNumber || !fieldValidator(phoneNumber, 'phone number')) {
            throw Error('Phone number is uncorrect');
        }
        if (!password || !fieldValidator(password, 'password')) {
            throw Error('Password has to be at least 3 symbols');
        }
        res.data = { firstName, lastName, email, phoneNumber, password }
    } catch (err) {
        res.err = err;
        res.status(400);
    } finally {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    let newData = {};
    try {
        if (req.body.hasOwnProperty('id')) {
            throw Error('Uncorrect field')
        }
        if (firstName) {
            newData.firstName = firstName;
        }
        if (lastName) {
            newData.lastName = lastName;
        }
        if (email) {
            if (!fieldValidator(email, 'email')) {
                throw Error('User email is uncorrect');
            }
            newData.email = email;
        }
        if (phoneNumber) {
            if (!fieldValidator(phoneNumber, 'phone number')) {
                throw Error('Phone number is uncorrect');
                newData.phoneNumber = phoneNumber;
            }
        }
        if (password) {
            if (!fieldValidator(password, 'password')) {
                throw Error('Passowrd has to be at least 3 symbols')
            }
            newData.password = password;
        }
        res.data = newData;
    } catch (err) {
        res.err = err;
        res.status(400);
    } finally {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;