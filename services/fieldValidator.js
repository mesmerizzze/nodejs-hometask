const fieldValidator = (field, type) => {
  switch (type) {
    case 'email': {
      if (/^\w+([\.-]?\w+)*@gmail.com$/.test(field)) {
        return true
      }
      break;
    }
    case 'phone number': {
      if (/^\+380[0-9]{9}$/.test(field)) {
        return true
      }
      break;
    }
    case 'password': {
      if (field.length >= 3) {
        return true
      }
    }
    default: {
      return false
    }
  }
}

exports.fieldValidator = fieldValidator;