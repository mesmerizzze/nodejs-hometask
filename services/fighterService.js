const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    deleteFighter(id) {
        const deletedFighter = FighterRepository.delete(id);
        if (!deletedFighter) {
            throw Error('Fighter does not exist');
        }
        return deletedFighter;
    }

    create(fighterData) {
        const newFighter = FighterRepository.create(fighterData)
        if (!newFighter) {
            throw Error('New fighter was not created');
        }
        return newFighter
    }

    getFighter(searchData) {
        const fighter = FighterRepository.getOne(searchData);
        if (!fighter) {
            throw Error('Fighter was not found');
        }
        return fighter;
    }

    getAllFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters || fighters.length === 0) {
            throw Error('No fighters found');
        }
        return fighters
    }

    updateFighterInfo(id, info) {
        const updatedFighter = FighterRepository.update(id, info);
        if (!updatedFighter) {
            throw Error('Fighter info was not updated');
        }
        return updatedFighter;
    }
}

module.exports = new FighterService();