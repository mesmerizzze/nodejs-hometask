const { UserRepository } = require('../repositories/userRepository');

class UserService {
    deleteUser(id) {
        const deletedUser = UserRepository.delete(id);
        if (deletedUser.length === 0) {
            throw Error('User does not exist');
        }
        return deletedUser;
    }

    updateUserInfo(id, info) {
        if (UserRepository.getOne(id)) {
            const updatedUser = UserRepository.update(id, info);
            if (!updatedUser) {
                throw Error('User info was not updated');
            }
            return updatedUser;
        } else {
            throw Error('User does not exist');
        }
    }

    signup(userData) {
        const item = UserRepository.create(userData);
        if (!item) {
            throw Error('New user was not created');
        }
        return item
    }

    getAllUsers() {
        const users = UserRepository.getAll();
        if (!users || users.length === 0) {
            throw Error('No users found');
        }
        return users
    }

    getUser(search) {
        const user = this.search(search);
        if (!user) {
            throw Error('User was not found');
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();