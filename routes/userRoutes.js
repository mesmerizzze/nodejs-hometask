const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getAllUsers();
    } catch (err) {
        res.err = err;
        res.status(404)
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params;
        res.data = UserService.getUser(id);
    } catch (err) {
        res.err = err;
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        if (res.data) {
            const newUser = UserService.signup(res.data);
            res.data = newUser;
        }
    } catch (err) {
        res.err = err;
        res.status(400);
    }
    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (res.data) {
            const { id } = req.params;
            const updatedData = UserService.updateUserInfo(id, res.data);
            res.data = updatedData;
        }
    } catch (err) {
        res.err = err;
        res.status(400);
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        res.data = UserService.deleteUser(id);
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;