const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    res.data = FighterService.getAllFighters();
  } catch (err) {
    res.err = err;
    res.status(404);
  } finally {
    next();
  }
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
  try {
    const { id } = req.params;
    res.data = FighterService.getFighter({ id });
  } catch (err) {
    res.err = err;
    res.status(404);
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', createFighterValid, async (req, res, next) => {
  try {
    if (res.data) {
      res.data = await FighterService.create(res.data);
    }
  } catch (err) {
    res.err = err;
    res.status(400);
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    if (res.data) {
      const { id } = req.params;
      const updatedData = FighterService.updateFighterInfo(id, res.data);
      res.data = updatedData;
    }
  } catch (err) {
    res.err = err;
    res.status(400);
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
  try {
    const { id } = req.params;
    res.data = FighterService.deleteFighter(id);
  } catch (err) {
    res.err = err;
    res.status(404);
  } finally {
    next();
  }
}, responseMiddleware)

module.exports = router;